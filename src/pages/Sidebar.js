import React from 'react';
import {
  CDBSidebar,
  CDBSidebarContent,
  CDBSidebarFooter,
  CDBSidebarHeader,
  CDBSidebarMenu,
  CDBSidebarMenuItem,
} from 'cdbreact';

import { NavLink } from 'react-router-dom';

// const Sidebar = () => {
//   return <div></div>;
// };

// const Sidebar = () => {
//     return (
//       <div style={{ display: 'flex', height: '100vh', overflow: 'scroll initial' }}>
//         <CDBSidebar textColor="#fff" backgroundColor="#333">
//           <CDBSidebarHeader prefix={<i className="fa fa-bars fa-large"></i>}>
//             <a href="/" className="text-decoration-none" style={{ color: 'inherit' }}>
//               Sidebar
//             </a>
//           </CDBSidebarHeader>
  
//           <CDBSidebarFooter style={{ textAlign: 'center' }}>
//             <div
//               className="sidebar-btn-wrapper"
//               style={{
//                 padding: '20px 5px',
//               }}
//             >
//               Sidebar Footer
//             </div>
//           </CDBSidebarFooter>
//         </CDBSidebar>
//       </div>
//     );
//   };

const Sidebar = () => {
    return (
      <div style={{ display: 'flex', height: '100vh', overflow: 'scroll initial' }}>
        <CDBSidebar textColor="#fff" backgroundColor="#333">
          <CDBSidebarHeader prefix={<i className="fa fa-bars fa-large"></i>}>
            <a href="/" className="text-decoration-none" style={{ color: 'inherit' }}>
              Centerious
            </a>
          </CDBSidebarHeader>
  
          <CDBSidebarContent className="sidebar-content">
            <CDBSidebarMenu>
              <NavLink exact to="/" activeClassName="activeClicked">
                <CDBSidebarMenuItem icon="columns">Dashboard</CDBSidebarMenuItem>
              </NavLink>
              <NavLink exact to="/Staff" activeClassName="activeClicked">
                <CDBSidebarMenuItem icon="table">Staff</CDBSidebarMenuItem>
              </NavLink>
              <NavLink exact to="/Students" activeClassName="activeClicked">
                <CDBSidebarMenuItem icon="table">Students</CDBSidebarMenuItem>
              </NavLink>
              <NavLink exact to="/Vehicles" activeClassName="activeClicked">
                <CDBSidebarMenuItem icon="table">Vehicles</CDBSidebarMenuItem>
              </NavLink>
              <NavLink exact to="/Routes" activeClassName="activeClicked">
                <CDBSidebarMenuItem icon="table">Routes</CDBSidebarMenuItem>
              </NavLink>
              <NavLink exact to="/Drivers" activeClassName="activeClicked">
                <CDBSidebarMenuItem icon="table">Drivers</CDBSidebarMenuItem>
              </NavLink>
              <NavLink exact to="/Exams" activeClassName="activeClicked">
                <CDBSidebarMenuItem icon="table">Exams</CDBSidebarMenuItem>
              </NavLink>
              <NavLink exact to="/Attendance" activeClassName="activeClicked">
                <CDBSidebarMenuItem icon="table">Attendance</CDBSidebarMenuItem>
              </NavLink>
              <NavLink exact to="/Finance" activeClassName="activeClicked">
                <CDBSidebarMenuItem icon="table">Finance</CDBSidebarMenuItem>
              </NavLink>
              <NavLink exact to="/ProgressReport" activeClassName="activeClicked">
                <CDBSidebarMenuItem icon="table">Progress Report</CDBSidebarMenuItem>
              </NavLink>
              <NavLink exact to="/DeletedMembers" activeClassName="activeClicked">
                <CDBSidebarMenuItem icon="table">Deleted Members</CDBSidebarMenuItem>
              </NavLink>
              {/* <NavLink exact to="/blogs" activeClassName="activeClicked">
                <CDBSidebarMenuItem icon="table">Tables</CDBSidebarMenuItem>
              </NavLink>
              <NavLink exact to="/profile" activeClassName="activeClicked">
                <CDBSidebarMenuItem icon="user">Profile page</CDBSidebarMenuItem>
              </NavLink>
              <NavLink exact to="/analytics" activeClassName="activeClicked">
                <CDBSidebarMenuItem icon="chart-line">Analytics</CDBSidebarMenuItem>
              </NavLink>
  
              <NavLink exact to="/hero404" target="_blank" activeClassName="activeClicked">
                <CDBSidebarMenuItem icon="exclamation-circle">404 page</CDBSidebarMenuItem>
              </NavLink> */}
            </CDBSidebarMenu>
          </CDBSidebarContent>
  
          <CDBSidebarFooter style={{ textAlign: 'center' }}>
            <div
              style={{
                padding: '20px 5px',
              }}
            >
              Sidebar Footer
            </div>
          </CDBSidebarFooter>
        </CDBSidebar>
      </div>
    );
  };

export default Sidebar;