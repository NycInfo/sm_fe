import React, { Component } from "react";
import './login.css'
import Sidebar from "./Sidebar";
import { useNavigate } from "react-router-dom";

class Login extends Component{
    constructor(){
        super()
        this.state = {
            username:"",
            password:"",
            errorMsg:""
        }
    }
    render(){
        return <div class="row">
                    <div class="col-md-8">
                        <h1>Login {this.state.username}</h1>
                        <label> User Name</label>
                        <input type="text" value={this.state.username} onChange={this.usernameHandler}/>
                        <label>Password</label>
                        <input type="password" value={this.state.password} onChange={this.passwordHandler}/>
                        <button onClick={this.loginSubmit}>Login</button>
                        <span class="errormsg">{this.state.errorMsg}</span>
                    </div>
        </div>
    }
    usernameHandler = (event) => {
        
        console.log(event)
        this.setState({username:event.target.value})
    }
    passwordHandler = (event) => {
        this.setState({password:event.target.value})
    }
    loginSubmit = (event) => {
        if(this.state.username == ""){
            this.setState({errorMsg:"User Name Required"})
            setTimeout(() => {
                this.setState({
                    errorMsg:""
                  })
                }, 3000);

        }else{
            this.setState({errorMsg:""})
            const navigate = useNavigate();
            navigate("/blogs")
            
        }
    }
}

export default Login;