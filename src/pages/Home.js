import Sidebar from "./Sidebar";
import './home.css'

const Home = () => {
    return <div class="row">
        <div class="headerdiv"></div>
                    <div class="col-md-3">
                        <Sidebar />
                    </div>
                    <div class="col-md-9">
                        <h2>Home</h2>
                    </div>
        </div>;
  };
  
  export default Home;