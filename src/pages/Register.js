import React, { Component } from "react";


class Register extends Component {
    constructor(){
        super()
        this.state = {
            firstname:"",
            lastname:"KN"
        }
    }
    render(){
        return <div>
            <h1>Registration Page</h1>
            <label>First Name</label>
            <input type="text" value={this.state.firstname} onChange={this.firstNameHandler}/>
            <label>Last Name</label>
            <input type="text" value={this.state.lastname} />
            <label>Mobile Number</label>
            <input type="text" />
            <button onClick={this.registerSubmit}>Save</button>
        </div>
    }
    firstNameHandler = (event) => {
        this.setState({firstname:event.target.value})
    }
    registerSubmit = (event) => {
        alert(`${this.state.firstname},${this.state.lastname}`)
    }
}

export default Register;